module Counter.Main where

import Prelude

import Effect (Effect)
import Flame (Html, QuerySelector(..), Subscription)
import Flame.Application.NoEffects as FAN
import Flame.Html.Attribute as HA
import Flame.Html.Element as HE

-- | The model represents the state of the app
type Model = Int

-- | Data type used to represent events
data Message = Increment | Decrement

-- | Initial state of the app
init :: Model
init = 0

-- | `update` is called to handle events
update :: Model -> Message -> Model
update model = case _ of
      Increment -> model + 5
      Decrement -> model - 3

-- | `view` is called whenever the model is updated
view :: Model -> Html Message
view model = HE.main "main" [
      HE.div [HA.class' "columns"] [            
            HE.div [HA.class' "column"] [            
                  HE.button [HA.class' "button", HA.onClick Decrement] "-"
            ],
            HE.div [HA.class' "column"] [
                  HE.div [HA.class' "content"] [         
                  HE.text $ show model
                  ]
            ],
            HE.div [HA.class' "column"] [            
                  HE.button [HA.class' "button", HA.onClick Increment] "+"
            ]
      ]     
]

-- | Events that come from outside the `view`
subscribe :: Array (Subscription Message)
subscribe = []

-- | Mount the application on the given selector
main :: Effect Unit
main = FAN.mount_ (QuerySelector "body") {
      init,
      view,
      update,
      subscribe
}